# Πρότυπο για προσωπικές συλλογές εκπαιδευτικών ιστοεφαρμογών

1. Κατεβάστε και αποσυμπιέστε το collection.tgz.
2. Τοποθετήστε τον φάκελο collection σε όποια θέση προτιμάτε, για παράδειγμα C:\Software\Collection στα Windows ή /home/administrator/collection στο Linux.
3. Εκτελέστε το start.bat στα Windows ή το start.sh στο Linux.
